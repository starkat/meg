use log::{error, trace};
use std::{
    fs::File,
    io::{self, Read, Seek},
    path::Path,
};

const SMD_HEADER_SIZE: usize = 512;
const SMD_BLOCK_SIZE: usize = 0x4000;
const SMD_MAGIC_NUMBER1: u8 = 0x03; // Offset 1
const SMD_MAGIC_NUMBER2: u8 = 0xAA; // Offset 8
const SMD_MAGIC_NUMBER3: u8 = 0xBB; // Offset 9

#[derive(Debug, Clone)]
#[repr(transparent)]
pub struct RomFile(Vec<u8>);

impl RomFile {
    pub fn load(path: &Path) -> io::Result<RomFile> {
        let mut file = File::open(path)?;
        let filesize = file.metadata()?.len() as usize;
        let mut buffer;

        // Try to sniff out an SMD header
        let mut header = [0; 10];
        file.read_exact(&mut header)?;
        if header[1] == SMD_MAGIC_NUMBER1
            && header[8] == SMD_MAGIC_NUMBER2
            && header[9] == SMD_MAGIC_NUMBER3
            && header[3..8]
                .iter()
                .cloned()
                .eq(std::iter::repeat(0u8).take(5))
        {
            trace!("{} is an SMD file", path.to_string_lossy());
            // Make sure it's not a split SMD
            if header[2] != 0 {
                error!(
                    "{} is a split SMD file and is not supported",
                    path.to_string_lossy()
                );
                return Err(io::ErrorKind::InvalidData.into());
            }

            // Skip the header
            buffer = Vec::with_capacity(filesize - SMD_HEADER_SIZE);
            buffer.resize(buffer.capacity(), 0);
            file.seek(io::SeekFrom::Start(SMD_HEADER_SIZE as u64))?;
            let mut pos = 0usize;

            // Read SMD in blocks
            let mut block = [0; SMD_BLOCK_SIZE];
            loop {
                match file.read_exact(&mut block) {
                    Ok(_) => {
                        // Deinterlace the block
                        for low in 0..SMD_BLOCK_SIZE / 2 {
                            let high = SMD_BLOCK_SIZE / 2 + low;
                            buffer[pos] = block[low];
                            buffer[pos + 1] = block[high];
                            pos += 1;
                        }
                    }
                    Err(ref e) if e.kind() == io::ErrorKind::UnexpectedEof => {
                        break;
                    }
                    Err(e) => {
                        return Err(e);
                    }
                }
            }
        } else {
            // Simple for regular rom files
            buffer = Vec::with_capacity(filesize);
            // Copy already read portion
            io::copy(&mut &header[..], &mut buffer)?;
            buffer.resize(buffer.capacity(), 0);

            file.read_to_end(&mut buffer)?;
        }

        trace!(
            "read {} bytes of file {}",
            buffer.len(),
            path.to_string_lossy()
        );

        Ok(RomFile(buffer))
    }
}
