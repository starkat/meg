mod config;
mod rom;

use crate::config::{Config, ConsoleOutput};
use crate::rom::RomFile;
use clap::{
    app_from_crate, crate_authors, crate_description, crate_name, crate_version, AppSettings, Arg,
};
use flexi_logger::{LogSpecBuilder, LogSpecification, LogTarget, Logger};
use log::trace;
use regex::Regex;
use std::{env, error::Error, path::Path};

fn main() -> Result<(), Box<dyn Error>> {
    include_str!("../Cargo.toml"); // For app_from_crate! compile dependency
    let args = app_from_crate!(";")
        .setting(AppSettings::ColoredHelp)
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .alias("cfg")
                .value_name("FILE")
                .help("The name of the configuration file")
                .default_value("config.toml")
                .env("MEG_CONFIG"),
        )
        .arg(
            Arg::with_name("logdir")
                .long("log-dir")
                .value_name("PATH")
                .help("The directory path to write log files to")
                .env("MEG_LOG_DIR"),
        )
        .arg(
            Arg::with_name("logstderr")
                .short("L")
                .long("log-console")
                .value_name("LOG-LEVEL")
                .possible_values(&ConsoleOutput::variants())
                .case_insensitive(true)
                .help("What log level to output to stderr console.")
                .env("MEG_LOG_STDERR"),
        )
        .arg(
            Arg::with_name("romfile")
                .index(1)
                .value_name("ROMFILE")
                .help("The name of the ROM file to execute"),
        )
        .get_matches();

    let config = config::load(&args)?;
    start_logger(&config)?;

    trace!("meg startup");

    if let Some(path) = args.value_of_os("romfile").map(|v| Path::new(v)) {
        let _romfile = RomFile::load(path);
    }

    Ok(())
}

fn start_logger(config: &Config) -> Result<(), Box<dyn Error>> {
    // Environment vars overwrite other logging config
    let logspec = if env::var_os("RUST_LOG").is_some() {
        LogSpecification::env()?
    } else {
        let mut builder = LogSpecBuilder::new();
        builder.default(config.log.level);
        for (modname, &level) in config.log.modules.iter() {
            builder.module(modname, level);
        }

        if let Some(filter) = config.log.filter.as_ref() {
            builder.finalize_with_textfilter(Regex::new(filter)?)
        } else {
            builder.finalize()
        }
    };

    let mut logger = Logger::with(logspec)
        .log_target(LogTarget::File)
        .o_directory(config.log.directory.clone())
        .o_timestamp(config.log.file_write == config::LogFileWriting::Timestamp)
        .o_append(config.log.file_write == config::LogFileWriting::Append)
        .o_discriminant(config.log.file_prefix.clone())
        .duplicate_to_stderr(config.log.stderr.into());
    if let Some(suffix) = config.log.file_suffix.clone() {
        logger = logger.suffix(suffix);
    }
    match config.log.format {
        config::LogFormat::Detailed => logger = logger.format(flexi_logger::detailed_format),
        config::LogFormat::Opt => logger = logger.format(flexi_logger::opt_format),
        config::LogFormat::Thread => logger = logger.format(flexi_logger::with_thread),
        config::LogFormat::Default => {}
    }

    logger.start()?;
    Ok(())
}
