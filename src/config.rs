use clap::{arg_enum, value_t, ArgMatches};
use flexi_logger::Duplicate;
use log::LevelFilter;
use serde::{Deserialize, Serialize};
use std::{collections::HashMap, error::Error, fs::File, io::Read};

#[derive(Serialize, Deserialize)]
#[serde(default)]
pub struct Config {
    pub log: LogConfig,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(default, rename_all = "kebab-case")]
pub struct LogConfig {
    #[serde(with = "LevelFilterDef")]
    pub level: LevelFilter,
    #[serde(with = "modules_filter_serde")]
    pub modules: HashMap<String, LevelFilter>,
    pub filter: Option<String>,
    pub directory: Option<String>,
    pub file_write: LogFileWriting,
    pub file_prefix: Option<String>,
    pub file_suffix: Option<String>,
    pub stderr: ConsoleOutput,
    pub format: LogFormat,
}

#[derive(Serialize, Deserialize, PartialEq, Clone, Copy, Debug)]
#[serde(rename_all = "lowercase")]
pub enum LogFileWriting {
    Truncate,
    Append,
    Timestamp,
}

#[derive(Serialize, Deserialize, PartialEq, Clone, Copy, Debug)]
#[serde(rename_all = "lowercase")]
pub enum LogFormat {
    Default,
    Opt,
    Detailed,
    Thread,
}

arg_enum! {
    #[derive(Serialize, Deserialize, PartialEq, Clone, Copy, Debug)]
    #[serde(rename_all = "lowercase")]
    pub enum ConsoleOutput {
        None,
        Error,
        Warn,
        Info,
        Debug,
        Trace,
        All,
    }
}

#[derive(Serialize, Deserialize)]
#[serde(remote = "LevelFilter", rename_all = "lowercase")]
enum LevelFilterDef {
    Off,
    Error,
    Warn,
    Info,
    Debug,
    Trace,
}

pub fn load(args: &ArgMatches) -> Result<Config, Box<dyn Error>> {
    let mut config = Config::default();

    // Read config file if provided and it exists
    if let Some(path) = args.value_of_os("config") {
        if let Ok(mut file) = File::open(path) {
            let mut buffer = String::new();
            file.read_to_string(&mut buffer)?;
            config = toml::de::from_str(&buffer)?;
        }
    }

    // Override config with command line args
    if args.is_present("logdir") {
        config.log.directory = args.value_of("logdir").map(|s| s.to_owned());
    }
    if args.is_present("logstderr") {
        config.log.stderr = value_t!(args, "logstderr", ConsoleOutput)?;
    }

    Ok(config)
}

impl Default for Config {
    fn default() -> Self {
        Config {
            log: LogConfig::default(),
        }
    }
}

impl Default for LogConfig {
    fn default() -> Self {
        LogConfig {
            level: LevelFilter::Off,
            modules: HashMap::new(),
            filter: None,
            directory: None,
            file_write: LogFileWriting::Truncate,
            file_prefix: None,
            file_suffix: None,
            stderr: ConsoleOutput::None,
            format: LogFormat::Default,
        }
    }
}

impl Default for ConsoleOutput {
    fn default() -> Self {
        ConsoleOutput::None
    }
}

impl From<ConsoleOutput> for Duplicate {
    fn from(co: ConsoleOutput) -> Self {
        match co {
            ConsoleOutput::None => Duplicate::None,
            ConsoleOutput::Error => Duplicate::Error,
            ConsoleOutput::Warn => Duplicate::Warn,
            ConsoleOutput::Info => Duplicate::Info,
            ConsoleOutput::Debug => Duplicate::Debug,
            ConsoleOutput::Trace => Duplicate::Trace,
            ConsoleOutput::All => Duplicate::All,
        }
    }
}

// Remote serialization of `HashMap<String, LevelFilter>`
mod modules_filter_serde {
    use log::LevelFilter;
    use serde::{
        de::{Deserializer, MapAccess, Visitor},
        ser::{SerializeMap, Serializer},
        Deserialize, Serialize,
    };
    use std::collections::HashMap;

    #[derive(Serialize, Deserialize)]
    struct LevelFilterHelper(#[serde(with = "super::LevelFilterDef")] LevelFilter);

    struct ModulesFilterVisitor;

    impl From<LevelFilterHelper> for LevelFilter {
        fn from(def: LevelFilterHelper) -> LevelFilter {
            def.0
        }
    }

    impl<'de> Visitor<'de> for ModulesFilterVisitor {
        type Value = HashMap<String, LevelFilter>;

        fn expecting(&self, formatter: &mut std::fmt::Formatter) -> std::fmt::Result {
            formatter.write_str("a map of module level filters")
        }

        fn visit_map<M>(self, mut access: M) -> Result<Self::Value, M::Error>
        where
            M: MapAccess<'de>,
        {
            let mut map = HashMap::with_capacity(access.size_hint().unwrap_or(0));

            while let Some((key, value)) = access.next_entry::<_, LevelFilterHelper>()? {
                map.insert(key, value.into());
            }
            Ok(map)
        }
    }

    pub fn deserialize<'de, D>(deserializer: D) -> Result<HashMap<String, LevelFilter>, D::Error>
    where
        D: Deserializer<'de>,
    {
        deserializer.deserialize_map(ModulesFilterVisitor {})
    }

    pub fn serialize<S>(
        modules: &HashMap<String, LevelFilter>,
        serializer: S,
    ) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        let mut map = serializer.serialize_map(Some(modules.len()))?;

        for (key, &value) in modules {
            map.serialize_entry(key, &LevelFilterHelper(value))?;
        }
        map.end()
    }
}
